<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Fitxer pUJAT</title>
		<link rel="stylesheet" href="formulari2.css">
  </head>
  <body>
    <header>
      <h1>Fitxer Pujat</h1>
    </header>

    <?php
      $linies = file($_FILES["fitxer"]["tmp_name"]);
      $fitxer = fopen('fitxers/fitxer.txt','w');

      foreach ($linies as $nLinia => $linia){
        echo $linia . "<br>";
        fwrite($fitxer, $linia. "<br>");
      }
      echo "<br>";
      $data = date("D/M/Y H:i:s");
      fwrite($fitxer, "Text pujat el: " . $data);
      fclose($fitxer);

      echo "El fitxer s'ha pujat correctament <br>";

      echo "Text del fitxer pujat: <br>";
      $file = file('fitxers/fitxer.txt');
      foreach ($file as $nlin=>$linia){
        echo $linia."<br>";
      }
    ?>

		<footer>
		  <p>Fet per: Álvaro Barba Sandoval</p>
		</footer>
  </body>
</html>
